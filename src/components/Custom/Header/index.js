import { Dimensions, Platform, View, TouchableOpacity, Text, Image, } from "react-native";
import React, { Component } from "react";
import NavigationService from "../../../utils/navigationService";
import Images from '../../../utils/images'
import styles from './styles'


class Header extends Component {

  renderToolbar = () => {
    return (
      <View style={this.props.isTransparent ? styles.transparentToolbar : styles.toolbar}>
        <View style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          paddingHorizontal: 10
        }}>
          <View style={{
            width: '15%', alignItems: 'center', justifyContent: 'center',
          }}>
            {this.props.noBack ? null : this.renderBackButton()}
          </View>
          <View style={{
            width: '70%', alignItems: 'center',
          }}>
            {this.props.titleView ? this.props.titleView() :
              <Text style={styles.headerTitleName} >{this.props.title}</Text>}
          </View>
          <View style={{ width: '15%' }}>
            {this.props.rightImg ?
              <TouchableOpacity style={styles.headerButton} onPress={() => this.props.fireEvent ? this.props.fireEvent() : {}}>
                <Image source={this.props.rightImg ? this.props.rightImg : null} style={{ width: 20, height: 20, resizeMode: "contain" }}></Image>
              </TouchableOpacity> : null
            }
          </View>
        </View>
      </View >
    )
  }

  onBackClick = () => {
    NavigationService.goBack();
  }

  renderBackButton = () => {
    return (
      <TouchableOpacity activeOpacity={0.8}
        style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' }}
        onPress={() => this.onBackClick()}>
        <Image source={Images.ic_back_white} style={{ width: 15, height: 15, resizeMode: "contain" }}></Image>
        <Text style={styles.backBtnText}> Back</Text>
      </TouchableOpacity>
    )
  }

  render() {
    return (
      this.renderToolbar()

    )
  }
}


export default Header


