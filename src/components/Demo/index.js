import React, { Component } from 'react'
import {
  SafeAreaView,
  View,
} from 'react-native';
import styles from './styles'
import colors from '../../utils/colors'
import Header from '../Custom/Header'

class Demo extends Component {
  render() {
    return (
      <View style={styles.container}>
        <SafeAreaView backgroundColor={colors.colorPrimary} />
        <Header title={"Demo"} />
        <View style={styles.container}>
        </View>
      </View>
    )
  }
}

export default Demo



