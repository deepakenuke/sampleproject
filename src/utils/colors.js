export default colors = {
  colorPrimary: '#2a62fd',
  colorPrimaryDark: '#2a2fff',
  colorAccent: 'pink',
  colorWhite: 'white',
  colorBlack: 'black',
  colorTransparent: 'transparent'
}