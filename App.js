/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StatusBar, View } from 'react-native';
import { Provider } from "react-redux";
import { PersistGate } from 'redux-persist/integration/react'
import { createAppContainer, createSwitchNavigator, } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import SplashScreen from 'react-native-splash-screen'

// Utilities import
import { store, persistor } from './src/redux/store/store'
import NavigationService from './src/utils/navigationService'
import colors from './src/utils/colors';

// Screen import
import Demo from './src/components/Demo'
import Authentication from './src/components/Authentication'

// Dashboard Stack
const DashboardStack = createStackNavigator({
  Demo: {
    screen: Demo
  },

}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

// Authentication Stack
const AuthenticationStack = createStackNavigator({
  Authentication: {
    screen: Authentication
  },
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

// SwitchNavigator
const SwitchNavigator = createAppContainer(
  createSwitchNavigator({
    DashboardStack: DashboardStack,
    AuthenticationStack: AuthenticationStack
  }, {
    initialRouteName: store.getState().isLoginReducer ? "DashboardStack" : "AuthenticationStack"
  })
);

// App Class
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentDidMount() {
    this.hideSplash()
  }

  hideSplash = () => {
    console.log("Hiding Splash")
    setTimeout(() => {
      SplashScreen.hide()
    }, 300)
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StatusBar barStyle="light-content" backgroundColor={colors.colorPrimary} />
          <SwitchNavigator ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }} />
        </PersistGate>
      </Provider>
    )
  }
}

export default App